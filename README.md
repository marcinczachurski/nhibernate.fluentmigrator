## NHibernate.FluentMigrator

NHibernate.FluentMigrator is simple library to create FluentMigration command from comparision model and database. 
It's similar to Add-Migration in EF but you have to run it from C# code.

#### Usage example

```csharp
// Real NHibernate configuration (with ConnectionString to DB and information about model)!!!
Configuration configuration = new Configuration();

// Creating class to generating migration.
var classGenerator = new SchemaClassGenerator(new SchemaGenerator(configuration));

// Saving file with migration.
string fullPath = classGenerator.SaveClassFile("Migrations/", "AddNewColumns", "Migrations");
```

You can write simple application with above lines and your NHibernate configuration.